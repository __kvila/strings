package main.test.com.symbolsandstrings.service;

import main.java.com.symbolsandstrings.service.SymbolsAndStringsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.Scanner;

public class SymbolsAndStringsServiceTest {
    private final Scanner scanner = Mockito.mock(Scanner.class);
    private final SymbolsAndStringsService cut = new SymbolsAndStringsService(scanner);

    static Arguments[] showReverseEnglishAlphabetTestArgs() {
        return new Arguments[]{
                Arguments.arguments("z y x w v u t s r q p o n m l k j i h g f e d c b a ")
        };
    }

    @ParameterizedTest
    @MethodSource("showReverseEnglishAlphabetTestArgs")
    void showReverseEnglishAlphabetTest(String expected) {

        String actual = cut.showReverseEnglishAlphabet();

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] showEnglishAlphabetTestArgs() {
        return new Arguments[]{
                Arguments.arguments("A B C D E F G H I J K L M N O P Q R S T U V W X Y Z ")
        };
    }

    @ParameterizedTest
    @MethodSource("showEnglishAlphabetTestArgs")
    void showEnglishAlphabetTest(String expected) {

        String actual = cut.showEnglishAlphabet();

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] showRuAlphabetTestArgs() {
        return new Arguments[]{
                Arguments.arguments("а б в г д е ж з и й к л м н о п р с т у ф х ц ч ш щ ъ ы ь э ю я ")
        };
    }

    @ParameterizedTest
    @MethodSource("showRuAlphabetTestArgs")
    void showRuAlphabetTest(String expected) {

        String actual = cut.showRuAlphabet();

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] showNumbersTestArgs() {
        return new Arguments[]{
                Arguments.arguments("0 1 2 3 4 5 6 7 8 9 ")
        };
    }

    @ParameterizedTest
    @MethodSource("showNumbersTestArgs")
    void showNumbersTest(String expected) {

        String actual = cut.showNumbers();

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] showASCIITableTestArgs() {
        return new Arguments[]{
                Arguments.arguments("! \" # $ % & ' ( ) * + , - . / 0 1 2 3 4 5 6 7 8 9 : ; < = > ? @ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z [ \\ ] ^ _ ` a b c d e f g h i j k l m n o p q r s t u v w x y z { | } ~ ")
        };
    }

    @ParameterizedTest
    @MethodSource("showASCIITableTestArgs")
    void showASCIITableTest(String expected) {

        String actual = cut.showASCIITable();

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] convertIntToStrTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, "5"),
                Arguments.arguments(0, "0"),
                Arguments.arguments(-2, "-2"),
                Arguments.arguments(123, "123")
        };
    }

    @ParameterizedTest
    @MethodSource("convertIntToStrTestArgs")
    void convertIntToStrTest(int num, String expected) {

        String actual = cut.convertIntToStr(num);

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] convertRealNumberToStringTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5.5, "5.5"),
                Arguments.arguments(0.3, "0.3"),
                Arguments.arguments(-2.23, "-2.23"),
                Arguments.arguments(123.241, "123.241")
        };
    }

    @ParameterizedTest
    @MethodSource("convertRealNumberToStringTestArgs")
    void convertRealNumberToStringTest(double num, String expected) {

        String actual = cut.convertRealNumberToString(num);

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] convertStrToIntTestArgs() {
        return new Arguments[]{
                Arguments.arguments("123", 123),
                Arguments.arguments("0", 0),
                Arguments.arguments("-254", -254)
        };
    }

    @ParameterizedTest
    @MethodSource("convertStrToIntTestArgs")
    void convertStrToIntTest(String str, int expected) {

        int actual = cut.convertStrToInt(str);

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] convertStringToRealNumberTestArgs() {
        return new Arguments[]{
                Arguments.arguments("123.321", 123.321),
                Arguments.arguments("0", 0),
                Arguments.arguments("-254.2", -254.2)
        };
    }

    @ParameterizedTest
    @MethodSource("convertStringToRealNumberTestArgs")
    void convertStringToRealNumberTest(String str, double expected) {

        double actual = cut.convertStringToRealNumber(str);

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getShortestWordTestArgs() {
        return new Arguments[]{
                Arguments.arguments("fsdjf sdfjsd sdf sdf dsfsdf df sdf", 2),
                Arguments.arguments("", 0),
                Arguments.arguments("a", 1)
        };
    }

    @ParameterizedTest
    @MethodSource("getShortestWordTestArgs")
    void getShortestWordTest(String expScanner, int expected) {
        Mockito.when(scanner.nextLine()).thenReturn(expScanner);

        int actual = cut.getShortestWord();

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] addSpaceAfterSignTestArgs() {
        return new Arguments[]{
                Arguments.arguments("sdfkfds.fsdfsdf? fdsf, sdfsdf,dsf,df,)fdsf", "sdfkfds. fsdfsdf? fdsf, sdfsdf, dsf, df, ) fdsf")
        };
    }

    @ParameterizedTest
    @MethodSource("addSpaceAfterSignTestArgs")
    void addSpaceAfterSignTest(String expScanner, String expected) {
        Mockito.when(scanner.nextLine()).thenReturn(expScanner);

        String actual = cut.addSpaceAfterSign();

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] removeTwinsTestArgs() {
        return new Arguments[]{
                Arguments.arguments("dsfffsdfewfew cvdkj 34324  fkwd 23423 2f32", "dsfew cvkj342")
        };
    }

    @ParameterizedTest
    @MethodSource("removeTwinsTestArgs")
    void removeTwinsTest(String expScanner, String expected) {
        Mockito.when(scanner.nextLine()).thenReturn(expScanner);

        String actual = cut.removeTwins();

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] countWordsQuantityTestArgs() {
        return new Arguments[]{
                Arguments.arguments("fdsjfs sdfjs s fkjs fksdjf sdkjf343 342 fd", 7)
        };
    }

    @ParameterizedTest
    @MethodSource("countWordsQuantityTestArgs")
    void countWordsQuantityTest(String expScanner, int expected) {
        Mockito.when(scanner.nextLine()).thenReturn(expScanner);

        int actual = cut.countWordsQuantity();

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] removeRangeTestArgs() {
        return new Arguments[]{
                Arguments.arguments("I live in Kiev!", 11, 4, "I live in !")
        };
    }

    @ParameterizedTest
    @MethodSource("removeRangeTestArgs")
    void removeRangeTest(String expScanner, int pos, int range, String expected) {
        Mockito.when(scanner.nextLine()).thenReturn(expScanner);

        String actual = cut.removeRange(pos, range);

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] reverseStringTestArgs() {
        return new Arguments[]{
                Arguments.arguments("I live in Kiev!", "!veiK ni evil I")
        };
    }

    @ParameterizedTest
    @MethodSource("reverseStringTestArgs")
    void reverseStringTest(String expScanner, String expected) {
        Mockito.when(scanner.nextLine()).thenReturn(expScanner);

        String actual = cut.reverseString();

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] deleteLastWordTestArgs() {
        return new Arguments[]{
                Arguments.arguments("I love programming!", "I love !")
        };
    }

    @ParameterizedTest
    @MethodSource("deleteLastWordTestArgs")
    void deleteLastWordTest(String expScanner, String expected) {
        Mockito.when(scanner.nextLine()).thenReturn(expScanner);

        String actual = cut.deleteLastWord();

        Assertions.assertEquals(expected, actual);
    }

        static Arguments[] changeToDollarTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new String[]{"dsfsdf", "sdfs", "df", "sfdsf", "sdqwe"}, 5, "dsfsdf sdfs df sf$$$ sd$$$ "),
                Arguments.arguments(new String[]{"dsfsdf", "sdfs", "df", "sfdsf", "sdqwe"}, 6, "dsf$$$ sdfs df sfdsf sdqwe "),
                Arguments.arguments(new String[]{"dsfsdf", "sdfs", "df", "sfdsf", "sdqwe"}, 4, "dsfsdf s$$$ df sfdsf sdqwe ")


        };
    }

    @ParameterizedTest
    @MethodSource("changeToDollarTestArgs")
    void changeToDollarTest(String [] strArray, int wordSize, String expected) {

        String actual = cut.changeToDollar(strArray, wordSize);

        Assertions.assertEquals(expected, actual);
    }
}
