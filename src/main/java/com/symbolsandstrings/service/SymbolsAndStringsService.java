package main.java.com.symbolsandstrings.service;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class SymbolsAndStringsService {

    private static final String MSG_ENGLISH_ALPHABET = "Symbols from A to Z in one line: ";
    private static final String MSG_REVERSE_ENGLISH_ALPHABET = "Symbols from z to a in one line: ";
    private static final String MSG_RU_ALPHABET = "Символы от а до я одной строкой: ";
    private static final String MSG_NUMBERS = "Numbers from 0 to 9 in one line: ";
    private static final String MSG_ASCII_TABLE = "Printable ASCII table range: ";
    private static final String MSG_ENTER_STRING = "Enter the string: ";
    private static final String MSG_SHORTEST_WORD_LENGTH = "Length of the shortest word: ";
    private static final String MSG_ENTER_WORD_SIZE = "Enter the size of the words to be changed: ";
    private static final String MSG_ENTER_WORD = "Enter a word №";
    private static final String MSG_WORD_ARRAY = "Your word array: ";
    private static final String MSG_CHANGER_WORD_ARRAY = "Changed word array: ";
    private static final String MSG_WORDS_QUANTITY = "User entered words: ";
    private static final String MSG_POSITION = "Enter the position from which to delete part of the string: ";
    private static final String MSG_RANGE = "Enter the line size you want to remove: ";
    private static final String MSG_INVALID_POSITION = "This position does not exist!";
    private static final String MSG_INVALID_RANGE = "Range is out of line!";
    private static final String MGS_CONVERSION_STRING = "String after conversion: ";


    Scanner scanner;

    public SymbolsAndStringsService(Scanner scanner){
        this.scanner = scanner;
    }

    public void runTasks(){
        //showEnglishAlphabet();
        //showReverseEnglishAlphabet();
        //showRuAlphabet();
        //showNumbers();
        //showASCIITable();
        //System.out.println(convertIntToStr(12345));
        //System.out.println(convertRealNumberToString(3.14));
        //System.out.println(convertStrToInt("23"));
        //System.out.println(convertStringToRealNumber("213.443"));
        //getShortestWord();
        //changeToDollar(5);
        //addSpaceAfterSign();
        //removeTwins();
        //countWordsQuantity();
        //removeRange();
        //reverseString();
        //deleteLastWord();
    }

    public String deleteLastWord(){
        System.out.println(MSG_ENTER_STRING);
        String str = scanner.nextLine();
        String [] strArr = str.split("");
        int firstLetter = 0, lastLetter = 0;

        Pattern pattern = Pattern.compile("[a-zA-Zа-яА-Я]+");
        Matcher matcher = pattern.matcher(str);

        while(matcher.find()){
            firstLetter = matcher.start();
            lastLetter = matcher.end();
        }

        str = "";
        for (int i = 0; (i < strArr.length); i++) {
            if(i >= firstLetter && i < lastLetter ){
                continue;
            }
            str += strArr[i];
        }

        return str;
    }

    public String reverseString(){
        System.out.println(MSG_ENTER_STRING);
        String str = scanner.nextLine();

        String [] strArr = str.split("");

        str = "";

        for (int i = strArr.length - 1; i >= 0; i--) {
            str += strArr[i];
        }

        return str;
    }

    public String removeRange(int pos, int myRange){
        System.out.println(MSG_ENTER_STRING);
        String str = scanner.nextLine();
        int position, range;


        System.out.println(MSG_POSITION);
        while(true) {
            position = pos;
            if(position < 1 || position > str.length()){
                System.out.println(MSG_INVALID_POSITION);
                continue;
            }
            break;
        }
        System.out.println(MSG_RANGE);
        while(true) {
            range = myRange;
            if(range > str.length() - position + 1){
                System.out.println(MSG_INVALID_RANGE);
                continue;
            }
            break;
        }

        String [] strArr = str.split("");
        str = "";

        for (int i = 0; i < strArr.length; i++) {
            if(i < position - 1 || i > position + range - 2){
                str += strArr[i];
            }
        }

        return str;
    }

    public int countWordsQuantity(){
        String str = scanner.nextLine(), newStr = "";
        String [] strArr;

        Pattern pattern = Pattern.compile("[a-zA-Zа-яА-Я]+");
        Matcher matcher = pattern.matcher(str);

        while(matcher.find()){
            newStr += str.substring(matcher.start(), matcher.end()) + " ";
        }

        strArr = newStr.split(" ", 0);

        if(strArr[0].length() == 0){
            return 0;
        } else {
            return strArr.length;
        }
    }

    public String removeTwins(){
        String str = scanner.nextLine();
        String [] strArr = str.split("");

        str = "";
        boolean flag = true;

        for (int i = 0; i < strArr.length; i++) {
            for (int j = 0; j < i; j++) {
                if(strArr[i].equals(strArr[j])){
                    flag = false;
                    break;
                }
            }

            if(flag){
                str += strArr[i];
            }

            flag = true;
        }

        return str;
    }

    public String addSpaceAfterSign(){
        String str = scanner.nextLine();

        char [] charArr = str.toCharArray();

        str = "";

        for (int i = 0; i < charArr.length; i++) {
            if((charArr[i] == '.' || charArr[i] == ',' || charArr[i] == '!' || charArr[i] == '?' || charArr[i] == '(' || charArr[i] == ')'
                    || charArr[i] == '-' || charArr[i] == '"' || charArr[i] == ':' || charArr[i] == ';') && (i + 1 != charArr.length) && (charArr[i + 1] != ' ')){
                str += charArr[i] + " ";
            } else {
                str += charArr[i];
            }
        }

        return str;
    }

    public String changeToDollar(String [] array, int wordSize){
        String newStr = "";
        String [] strArr = array;
        String [] lettersArr;

        for (int i = 0; i < strArr.length; i++) {
            if(strArr[i].length() == wordSize && strArr[i].length() >= 3){
                lettersArr = strArr[i].split("");
                strArr[i] = "";

                for (int j = lettersArr.length - 1; j > lettersArr.length - 4; j--) {
                    lettersArr[j] = "$";
                }
                for (int j = 0; j < lettersArr.length; j++) {
                    strArr[i] += lettersArr[j];
                }
            }
        }

        for (int i = 0; i < strArr.length; i++) {
            newStr += strArr[i] + " ";
        }

        return newStr;
    }

    public int getShortestWord(){
        System.out.println(MSG_ENTER_STRING);
        String str = scanner.nextLine(), newStr = "";
        String [] strArr;

        Pattern pattern = Pattern.compile("[a-zA-zа-яА-Я]+");
        Matcher matcher = pattern.matcher(str);

        while(matcher.find()){
           newStr += str.substring(matcher.start(), matcher.end()) + " ";
        }

        strArr = newStr.split(" ", 0);

        if(strArr[0].length() == 0){
            return 0;
        } else
            if(strArr.length == 1){
                return strArr[0].length();
            } else {
                int minWordLen = strArr[0].length();

                for (int i = 0; i < strArr.length; i++) {
                    if (minWordLen > strArr[i].length()) {
                        minWordLen = strArr[i].length();
                    }
                }
                return minWordLen;
            }
    }

    public double convertStringToRealNumber(String str){
        return Double.parseDouble(str);
    }

    public int convertStrToInt(String str){
        return Integer.parseInt(str);
    }

    public String convertRealNumberToString(double num){
        String str = "";
        return str += num;
    }

    public String convertIntToStr(int num){
        String str = "";
        return str += num;
    }

    public String showASCIITable(){
        String str = "";

        for (int i = 33; i < 127; i++) {
            str += (char)i + " ";
        }

        return str;
    }

    public String showNumbers(){
        String str = "";

        for (int i = 0; i < 10; i++) {
            str += i + " ";
        }

        return str;
    }

    public String showRuAlphabet(){
        String str = "";

        for (int i = 1072; i < 1104; i++) {
            str += (char)i + " ";
        }

        return str;
    }

    public String showEnglishAlphabet(){
        String str = "";

        for (int i = 65; i < 91; i++) {
            str += (char)i + " ";
        }

        return str;
    }

    public String showReverseEnglishAlphabet(){
        String str = "";

        for (int i = 122; i > 96; i--) {
            str += (char)i + " ";
        }

        return str;
    }
}
