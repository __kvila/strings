package main.java.com.symbolsandstrings;

import main.java.com.symbolsandstrings.service.SymbolsAndStringsService;

import java.util.Scanner;

public class SymbolsAndStringsRun {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        new SymbolsAndStringsService(scanner).runTasks();
    }
}
